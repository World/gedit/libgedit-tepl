libgedit-tepl
=============

Short description
-----------------

Gedit Technology - Text editor product line

Longer description
------------------

libgedit-tepl is part of
[Gedit Technology](https://gedit-text-editor.org/technology.html).

It is a library that eases the development of text editors and IDEs based on
GTK.

More information
----------------

[More information about libgedit-tepl](docs/more-information.md).
